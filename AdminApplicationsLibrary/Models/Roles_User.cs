﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminApplicationsLibrary
{

    //[role_id] [int] NOT NULL,
    //[user_id] [int] NOT NULL,
    //[created_at] [datetime] NULL,
    //[updated_at] [datetime] NULL,
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[lock_version] [int] NOT NULL CONSTRAINT [DF__roles_use__lock___7B264821]  DEFAULT ((0))

    [Table("roles_users")]
    [DisplayColumn("Id", "user_id")]
    public class Roles_User
        {

            [Key]
            [Display(Name = "Role")]
            public int role_id { get; set; }

            [Key]
            [Display(Name = "User")]
            public int user_id { get; set; }

            [DataType(DataType.DateTime)]
            [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
            [Display(Name = "Created")]
            public DateTime? created_at { get; set; }

            [DataType(DataType.DateTime)]
            [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
            [Display(Name = "Last Updated")]
            public DateTime? updated_at { get; set; }

            [Display(Name = "Created By")]
            public int? created_by { get; set; }

            [Display(Name = "Updated By")]
            public int? updated_by { get; set; }

            [ScaffoldColumn(false)]
            public int lock_version { get; set; }

        }

}
