﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminApplicationsLibrary
{

    //[id] [int] NOT NULL,
    //[name] [varchar](50) NOT NULL,
    //[description] [varchar](200) NULL,
    //[resource_id] [int] NOT NULL,
    //[created_at] [datetime] NOT NULL CONSTRAINT [DF_roles_created_at]  DEFAULT (getdate()),
    //[updated_at] [datetime] NOT NULL CONSTRAINT [DF_roles_updated_at]  DEFAULT (getdate()),
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[lock_version] [int] NOT NULL CONSTRAINT [DF__roles__lock_vers__6FB49575]  DEFAULT ((0)),

    [Table("roles")]
    [DisplayColumn("Id", "name")]
    public class Role
        {

            [Key]
            [Display(Name = "Id")]
            public int id { get; set; }

            [Display(Name = "Name")]
            [StringLength(50, MinimumLength = 1)]
            public string name { get; set; }

            [Display(Name = "Description")]
            [StringLength(200, MinimumLength = 0)]
            public string description { get; set; }

            [Display(Name = "Resource")]
            public int resource_id { get; set; }

            [DataType(DataType.DateTime)]
            [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
            [Display(Name = "Created")]
            public DateTime created_at { get; set; }

            [DataType(DataType.DateTime)]
            [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
            [Display(Name = "Last Updated")]
            public DateTime updated_at { get; set; }

            [Display(Name = "Created By")]
            public int? created_by { get; set; }

            [Display(Name = "Updated By")]
            public int? updated_by { get; set; }

            [ScaffoldColumn(false)]
            public int lock_version { get; set; }

        }

}
