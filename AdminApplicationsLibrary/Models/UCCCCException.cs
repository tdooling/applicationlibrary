﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminApplicationsLibrary
{
 
    //[Id] [int] IDENTITY(1,1) NOT NULL,
    //[ExceptionDate] [datetime] NOT NULL,
    //[HResult] [int] NOT NULL,
    //[Message] [varchar](1000) NOT NULL,
    //[Source] [varchar](1000) NOT NULL,
    //[StackTracce] [varchar](4000) NOT NULL,
    //[Module] [varchar](4000) NOT NULL,
    //[Name] [varchar](1000) NOT NULL

    [Table("UCCCCExceptions")]
    [DisplayColumn("Id", "ExceptionDate",true)]
    public class UCCCCException
    {

        [Key]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Created")]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime ExceptionDate { get; set; }

        [Display(Name = "HResult")]
        int HResult { get; set; }

        [Display(Name = "Message")]
        [StringLength(1000, MinimumLength = 1)]
        public string Message { get; set; }

        [Display(Name = "Source")]
        [StringLength(1000, MinimumLength = 1)]
        public string Source { get; set; }

        [Display(Name = "Stack Trace")]
        [StringLength(4000, MinimumLength = 1)]
        public string StackTrace { get; set; }

        [Display(Name = "Program")]
        [StringLength(4000, MinimumLength = 1)]
        public string Module { get; set; }

        [Display(Name = "Method Name")]
        [StringLength(4000, MinimumLength = 1)]
        public string Name { get; set; }

        public UCCCCException()
        {

        }

        public UCCCCException(Exception ex)
        {
            HResult = ex.HResult;
            Message = ex.Message;
            Source = ex.Source;
            StackTrace = ex.StackTrace;
            Module = ex.TargetSite.Module.Name;
            Name = ex.TargetSite.Name;
        }

    }
}
