﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminApplicationsLibrary
{
    [Table("view_member_lists")]
    [DisplayColumn("Id", "last_name")]
    public class MemberListViewModel
    {
        [Key]
        public int id { get; set; }
        public string chicago_id { get; set; }
        public int user_id { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50, MinimumLength = 1)]
        public string first_name { get; set; }

        [Display(Name = "Last Name")]
        [StringLength(50, MinimumLength = 1)]
        public string last_name { get; set; }
        
        [Display(Name = "Middle Initial")]
        [StringLength(10, MinimumLength = 0)]
        public string middle_initial { get; set; }
        
        [Display(Name = "Degrees")]
        [StringLength(50, MinimumLength = 0)]
        public string degrees { get; set; }
        
        [Display(Name = "Phone")]
        [StringLength(255, MinimumLength = 0)]
        public string phone { get; set; }

        [DataType(DataType.Date)] // added by TRD 10/5/2015
        //[DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Termination Date")]
        public DateTime termination_date { get; set; }
        
        [Display(Name = "Clinical")]
        public bool is_clinical { get; set; }
        
        [Display(Name = "Translational")]
        public bool is_translational { get; set; }
        
        [Display(Name = "Basic")]
        public bool is_basic { get; set; }
        
        [Display(Name = "Professorship")]
        [StringLength(255, MinimumLength = 0)]
        public string endowed_professorship { get; set; }
        
        [Display(Name = "Email")]
        [StringLength(100, MinimumLength = 1)]
        public string email { get; set; }
        
        [Display(Name = "Username")]
        [StringLength(50, MinimumLength = 1)]
        public string user_name { get; set; }
        
        [Display(Name = "CTRC Active")]
        public bool is_ctrc_active { get; set; }
        
        [Display(Name = "SAM Active")]
        public bool is_sam_active { get; set; }
        
        [Display(Name = "Active")]
        public bool is_membership_active { get; set; }
        
        [Display(Name = "Program Leader")]
        public bool is_program_leader { get; set; }
        
        [Display(Name = "Facility Director")]
        public bool is_facility_director { get; set; }
        
        [Display(Name = "CAC Member")]
        public bool is_cac_member { get; set; }
        
        [Display(Name = "Status")]
        [StringLength(50, MinimumLength = 1)]
        public string membership_status { get; set; }
        
        [Display(Name = "Program")]
        [StringLength(50, MinimumLength = 1)]
        public string program { get; set; }
        
        [Display(Name = "Rank")]
        [StringLength(50, MinimumLength = 1)]
        public string rank { get; set; }
        
        [Display(Name = "Department")]
        [StringLength(50, MinimumLength = 1)]
        public string department { get; set; }
        
        [Display(Name = "Section")]
        [StringLength(50, MinimumLength = 1)]
        public string section { get; set; }

    }
}