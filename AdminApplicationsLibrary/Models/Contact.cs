﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminApplicationsLibrary
{

    //[id] [int] IDENTITY(1,1) NOT NULL,
    //[first_name] [varchar](200) NULL,
    //[last_name] [varchar](200) NULL,
    //[email1] [varchar](200) NULL,
    //[email2] [varchar](200) NULL,
    //[phone1] [varchar](200) NULL,
    //[phone2] [varchar](200) NULL,
    //[contact_type] [varchar](200) NULL,
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[created_at] [datetime] NULL,
    //[updated_at] [datetime] NULL,


    [Table("contacts")]
    [DisplayColumn("Id", "last_name")]
    public class Contact
    {

        [Key]
        [Display(Name = "Id")]
        public int id { get; set; }

        [Display(Name = "First Name")]
        [StringLength(200, MinimumLength = 0)]
        public string first_name { get; set; }
        
        [Display(Name = "Last Name")]
        [StringLength(200, MinimumLength = 0)]
        public string last_name { get; set; }
        
        [Display(Name = "Main Email")]
        [StringLength(200, MinimumLength = 0)]
        public string email1 { get; set; }
        
        [Display(Name = "Alternate Email")]
        [StringLength(200, MinimumLength = 0)]
        public string email2 { get; set; }
        
        [Display(Name = "Main Phone")]
        [StringLength(200, MinimumLength = 0)]
        public string phone1 { get; set; }
        
        [Display(Name = "Alternate Phone")]
        [StringLength(200, MinimumLength = 0)]
        public string phone2 { get; set; }
        
        [Display(Name = "Contact Type")]
        [StringLength(200, MinimumLength = 0)]
        public string contact_type { get; set; }

        [Display(Name = "Created By")]
        public int? created_by { get; set; }

        [Display(Name = "Updated By")]
        public int? updated_by { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Created")]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime? created_at { get; set; }

        [Display(Name = "Last Updated")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime? updated_at { get; set; }

    }
}