﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;


namespace AdminApplicationsLibrary
{

    //[member_id] [int] NULL DEFAULT (NULL),
    //[publication_id] [int] NULL DEFAULT (NULL),
    //[created_at] [datetime] NOT NULL,
    //[updated_at] [datetime] NOT NULL,
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[lock_version] [int] NOT NULL DEFAULT ((0))

    [Table("members_publications")]
    [DisplayColumn("Id", "member_id")]
    [DataContract]
    public class Members_Publication
    {
        [Key]
        [DataMember]
        [Display(Name = "Member Id")]
        public int? member_id { get; set; }

        [Key]
        [DataMember]
        [Display(Name = "Publication Id")]
        public int? publication_id { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Created")]
        [DataMember]
        public DateTime created_at;

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Last Updated")]
        [DataMember]
        public DateTime updated_at { get; set; }

        [DataMember]
        [Display(Name = "Created By")]
        public int? created_by { get; set; }

        [DataMember]
        [Display(Name = "Updated By")]
        public int? updated_by { get; set; }

        [DataMember]
        [ScaffoldColumn(false)]
        public int lock_version { get; set; }

    }
}