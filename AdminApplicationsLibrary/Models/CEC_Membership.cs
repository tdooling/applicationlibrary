﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace AdminApplicationsLibrary
{

    //[id] [int] IDENTITY(1,1) NOT NULL,
    //[member_id] [int] NOT NULL,
    //[term_start_date] [datetime] NULL,
    //[term_end_date] [datetime] NULL,
    //[comments] [varchar](255) NULL,
    //[created_at] [datetime] NOT NULL,
    //[updated_at] [datetime] NOT NULL,
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[lock_version] [int] NOT NULL,


    [Table("cec_memberships")]
    [DisplayColumn("Id", "term_start_date", true)]
    public class CEC_Membership
    {
        [Key]
        [Display(Name = "Id")]
        public int id { get; set; }

        [Display(Name = "Member Id")]
        public int member_id { get; set; }

        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Term Start")]
        public DateTime? term_start_date { get; set; }

        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Term End")]
        public DateTime? term_end_date { get; set; }

        [Display(Name = "Comments")]
        [StringLength(255,MinimumLength=0)]
        public string comments { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Created")]
        public DateTime created_at;

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Last Updated")]
        public DateTime updated_at { get; set; }

        [Display(Name = "Created By")]
        public int? created_by { get; set; }

        [Display(Name = "Updated By")]
        public int? updated_by { get; set; }

        [ScaffoldColumn(false)]
        public int lock_version { get; set; }

    }
}