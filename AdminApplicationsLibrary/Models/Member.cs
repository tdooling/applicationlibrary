﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminApplicationsLibrary
{

    //[id] [int] IDENTITY(1,1) NOT NULL,
    //[chicago_id] [varchar](255) NULL,
    //[user_id] [int] NULL,
    //[first_name] [varchar](50) NOT NULL,
    //[last_name] [varchar](50) NOT NULL,
    //[middle_initial] [varchar](10) NULL,
    //[degrees] [varchar](50) NULL,
    //[search_name] [varchar](100) NULL,
    //[termination_date] [datetime] NULL,
    //[pi_pssn] [varchar](255) NULL,
    //[created_at] [datetime] NOT NULL,
    //[updated_at] [datetime] NOT NULL,
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[lock_version] [int] NOT NULL CONSTRAINT [DF__members__lock_ve__38996AB5]  DEFAULT ((0)),
    //[has_reviewed_publication] [datetime] NULL,
    //[has_confirmed_no_publication] [datetime] NULL,
    //[is_clinical] [bit] NULL,
    //[is_translational] [bit] NULL,
    //[is_basic] [bit] NULL,
    //[is_standing_review_committee] [bit] NULL CONSTRAINT [DF__members__is_stan__7F01C5FD]  DEFAULT ((0)),
    //[phone] [varchar](255) NULL,
    //[endowed_professorship] [varchar](255) NULL,
    //[current_photo_location] [varchar](255) NULL,

    [Table("contacts")]
    [DisplayColumn("Id", "last_name")]
    public class Member
    {
        [Key]
        [Display(Name = "Id")]
        public int id { get; set; }
        
        [Display(Name = "Chicago Id")]
        [StringLength(255, MinimumLength = 0)]
        public string chicago_id { get; set; }
        
        [Display(Name = "User Id")]
        public int? user_id { get; set; }
        
        [Display(Name = "First Name")]
        [StringLength(50, MinimumLength = 1)]
        public string first_name { get; set; }
        
        [Display(Name = "Last Name")]
        [StringLength(50, MinimumLength = 1)]
        public string last_name { get; set; }
        
        [Display(Name = "Middle Initial")]
        [StringLength(10, MinimumLength = 0)]
        public string middle_initial { get; set; }
        
        [Display(Name = "Degrees")]
        [StringLength(50, MinimumLength = 0)]
        public string degrees { get; set; }
        
        [Display(Name = "Search Name")]
        [StringLength(100, MinimumLength = 0)]
        public string search_name { get; set; }
        
        [Display(Name = "Termination Date")]
        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime? termination_date { get; set; }
        
        [Display(Name = "PI Pssn")]
        [StringLength(255, MinimumLength = 0)]
        public string pi_pssn { get; set; }

        [Display(Name = "Has Reviewed Publication")]
        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime? has_reviewed_publication { get; set; }
        
        [Display(Name = "Has Confirmed No Publication")]
        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime? has_confirmed_no_publication { get; set; }
        
        [Display(Name = "Clinical")]
        public Boolean? is_clinical { get; set; }
        
        [Display(Name = "Translational")]
        public Boolean? is_translational { get; set; }
        
        [Display(Name = "Basic")]
        public Boolean? is_basic { get; set; }
        
        [Display(Name = "Standing Review Committee")]
        public Boolean? is_standing_review_committee { get; set; }
        
        [Display(Name = "Phone")]
        [StringLength(255, MinimumLength = 0)]
        public string phone { get; set; }
        
        [Display(Name = "Endowed Professorship")]
        [StringLength(255, MinimumLength = 0)]
        public string endowed_professorship { get; set; }

        [Display(Name = "Photo URL")]
        [StringLength(255, MinimumLength = 0)]
        public string current_photo_location { get; set; }

        [Display(Name = "Created")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime created_at { get; set; }

        [Display(Name = "Last Updated")]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime updated_at { get; set; }

        [Display(Name = "Created By")]
        public int? created_by { get; set; }

        [Display(Name = "Updated By")]
        public int? updated_by { get; set; }

        [ScaffoldColumn(false)]
        public int lock_version { get; set; }

    }
}