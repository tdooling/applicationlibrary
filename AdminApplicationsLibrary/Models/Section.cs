﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminApplicationsLibrary
{
    //[id] [int] IDENTITY(1,1) NOT NULL,
    //[name] [varchar](50) NOT NULL,
    //[created_at] [datetime] NOT NULL,
    //[updated_at] [datetime] NOT NULL,
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[lock_version] [int] NOT NULL CONSTRAINT [DF__sections__lock_v__01D345B0]  DEFAULT ((0)),

    [Table("sections")]
    [DisplayColumn("Id", "name")]
    public class Section
    {
        [Key]
        [Display(Name = "Id")]
        public int id { get; set; }

        [Display(Name = "Name")]
        [StringLength(50, MinimumLength = 1)]
        public string name { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Created")]
        public DateTime created_at { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Last Updated")]
        public DateTime updated_at { get; set; }

        [Display(Name = "Created By")]
        public int? created_by { get; set; }

        [Display(Name = "Updated By")]
        public int? updated_by { get; set; }

        public int lock_version { get; set; }

    }
}