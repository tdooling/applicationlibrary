﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;


namespace AdminApplicationsLibrary
{

    //[id] [int] IDENTITY(1,1) NOT NULL,
    //[member_id] [int] NOT NULL,
    //[promotion_date] [datetime] NOT NULL,
    //[promotion_description] [varchar](255) NOT NULL,
    //[promotion_medium_id] [int] NOT NULL,
    //[media_link] [varchar](255) NULL,


    [Table("member_communications")]
    [DisplayColumn("Id", "last_name")]
    public class Member_Communication
    {
        [Key]
        [Display(Name = "Id")]
        public int id { get; set; }

        [Display(Name = "Member Id")]
        public int member_id { get; set; }

        [DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Promotion Date")]
        public DateTime promotion_date { get; set; }

        [Display(Name = "Description")]
        [StringLength(255,MinimumLength=1)]
        public string promotion_description { get; set; }

        [Display(Name = "Promotion Medium")]
        public int promotion_medium_id { get; set; }

        [Display(Name = "Media Link")]
        [DataType(DataType.Url)]
        [StringLength(255, MinimumLength = 0)]
        public string media_link { get; set; }

    }
}