﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminApplicationsLibrary
{

    //[id] [int] IDENTITY(1,1) NOT NULL,
    //[start_date] [datetime] NULL CONSTRAINT [DF__universit__start__04AFB25B]  DEFAULT (NULL),
    //[end_date] [datetime] NULL CONSTRAINT [DF__universit__end_d__05A3D694]  DEFAULT (NULL),
    //[member_id] [int] NOT NULL,
    //[rank_id] [int] NOT NULL,
    //[department_id] [int] NOT NULL,
    //[section_id] [int] NOT NULL,
    //[created_at] [datetime] NOT NULL,
    //[updated_at] [datetime] NOT NULL,
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[lock_version] [int] NOT NULL CONSTRAINT [DF__universit__lock___0880433F]  DEFAULT ((0)),


    [Table("university_appointments")]
    [DisplayColumn("Id", "start_date", true)]
    public class University_Appointment
    {
        [Key]
        [Display(Name = "Id")]
        public int id { get; set; }

        [DataType(DataType.Date)] // added by TRD 10/5/2015
        //[DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Start Date")]
        public DateTime? start_date { get; set; }
        
        [DataType(DataType.Date)] // added by TRD 10/5/2015
        //[DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "End Date")]
        public DateTime? end_date { get; set; }
        
        public int member_id { get; set; }
        
        [Display(Name = "Rank")]
        public int rank_id { get; set; }
        
        [Display(Name = "Department")]
        public int department_id { get; set; }
        
        [Display(Name = "Section")]
        public int section_id { get; set; }

        [Display(Name = "Created")]
        [DataType(DataType.DateTime)] // added by TRD 10/5/2015
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime created_at { get; set; }

        [Display(Name = "Last Updated")]
        [DataType(DataType.DateTime)] // added by TRD 10/5/2015
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime updated_at { get; set; }

        [Display(Name = "Created By")]
        public int? created_by { get; set; }

        [Display(Name = "Updated By")]
        public int? updated_by { get; set; }

        [ScaffoldColumn(false)]
        public int lock_version { get; set; }

        // the following fields are used for display purposes only:
        // they are not filled by the database access routines

        public string Rank;

        public string Department;

        public string Section;
    }
}