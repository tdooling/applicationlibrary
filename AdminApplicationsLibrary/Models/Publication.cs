﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace AdminApplicationsLibrary
{

    //[id] [int] IDENTITY(1,1) NOT NULL,
    //[pmid] [varchar](50) NULL,
    //[title] [varchar](4000) NOT NULL,
    //[publication_year] [varchar](4) NOT NULL,
    //[author_names] [varchar](max) NOT NULL,
    //[journal_title] [varchar](200) NULL,
    //[journal_issn] [varchar](50) NULL,
    //[journal_source] [varchar](200) NULL,
    //[created_at] [datetime] NOT NULL,
    //[updated_at] [datetime] NOT NULL,
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[lock_version] [int] NOT NULL CONSTRAINT [DF__publicati__lock___47DBAE45]  DEFAULT ((0)),
    //[affiliation] [varchar](4000) NULL,
    //[journal_volume] [varchar](255) NULL,
    //[journal_issue] [varchar](255) NULL,
    //[journal_pagination] [varchar](255) NULL,
    //[pmcid] [varchar](255) NULL,
    //[grant_number] [varchar](255) NULL,
    //[publication_month] [varchar](255) NULL,
    //[publication_day] [varchar](255) NULL,
    //[formatted_journal_source] [varchar](255) NULL,
    //[publication_status] [varchar](255) NULL,
    //[is_review_type] [bit] NULL,
    //[is_inter_institutional] [int] NULL,
    //[is_using_core] [int] NULL,

    [Table("publications")]
    [DisplayColumn("Id", "publication_year", true)]
    [DataContract]
    public class Publication
    {
        [Key]
        [Display(Name = "Id")]
        [DataMember]
        public int id { get; set; }

        [Display(Name = "PMID")]
        [StringLength(50, MinimumLength = 0)]
        [DataMember]
        public string pmid { get; set; }

        [Display(Name = "Title")]
        [StringLength(4000, MinimumLength = 1)]
        [DataMember]
        public string title { get; set; }

        [Display(Name = "Year")]
        [StringLength(4, MinimumLength = 1)]
        [DataMember]
        public string publication_year { get; set; }

        [Display(Name = "Authors")]
        [StringLength(4000, MinimumLength = 1)]
        [DataMember]
        public string author_names { get; set; }

        [Display(Name = "Journals")]
        [StringLength(200, MinimumLength = 0)]
        [DataMember]
        public string journal_title { get; set; }

        [Display(Name = "ISSN")]
        [StringLength(50, MinimumLength = 0)]
        [DataMember]
        public string journal_issn { get; set; }

        [Display(Name = "Source")]
        [StringLength(200, MinimumLength = 0)]
        [DataMember]
        public string journal_source { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Created")]
        public DateTime created_at;

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Last Updated")]
        public DateTime updated_at { get; set; }

        [Display(Name = "Created By")]
        [DataMember]
        public int? created_by { get; set; }

        [Display(Name = "Updated By")]
        [DataMember]
        public int? updated_by { get; set; }

        [ScaffoldColumn(false)]
        public int lock_version { get; set; }

        [Display(Name = "Affiliation")]
        [StringLength(4000, MinimumLength = 0)]
        [DataMember]
        public string affiliation { get; set; }

        [Display(Name = "Volume")]
        [StringLength(255, MinimumLength = 0)]
        [DataMember]
        public string journal_volume { get; set; }

        [Display(Name = "Issue")]
        [StringLength(255, MinimumLength = 0)]
        [DataMember]
        public string journal_issue { get; set; }

        [Display(Name = "Pagination")]
        [StringLength(255, MinimumLength = 0)]
        [DataMember]
        public string journal_pagination { get; set; }

        [Display(Name = "PMCID")]
        [StringLength(255, MinimumLength = 0)]
        [DataMember]
        public string pmcid { get; set; }

        [Display(Name = "Grant Number")]
        [StringLength(255, MinimumLength = 0)]
        [DataMember]
        public string grant_number { get; set; }

        [Display(Name = "Pub Month")]
        [StringLength(255, MinimumLength = 0)]
        [DataMember]
        public string publication_month { get; set; }

        [Display(Name = "Pub Day")]
        [StringLength(255, MinimumLength = 0)]
        [DataMember]
        public string publication_day { get; set; }

        [Display(Name = "Journal Source")]
        [StringLength(255, MinimumLength = 0)]
        [DataMember]
        public string formatted_journal_source { get; set; }

        [Display(Name = "Pub Status")]
        [StringLength(255, MinimumLength = 0)]
        [DataMember]
        public string publication_status { get; set; }

        [Display(Name = "Review Type")]
        [DataMember]
        public bool? is_review_type { get; set; }

        [Display(Name = "Inter Institutional")]
        [DataMember]
        public int? is_inter_institutional { get; set; }

        [Display(Name = "Using Core")]
        [DataMember]
        public bool? is_using_core { get; set; }

    }
}