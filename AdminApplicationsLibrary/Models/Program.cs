﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminApplicationsLibrary
{

    //[id] [int] NOT NULL,
    //[name] [varchar](50) NOT NULL,
    //[created_at] [datetime] NOT NULL,
    //[updated_at] [datetime] NOT NULL,
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[lock_version] [int] NOT NULL CONSTRAINT [DF__programs__lock_v__2A164134]  DEFAULT ((0)),
    //[program_number] [varchar](50) NOT NULL,


    [Table("programs")]
    [DisplayColumn("Id", "name", false)]
    public class Program
    {
        [Key]
        [Display(Name = "Id")]
        public int id { get; set; }

        [Display(Name = "Name")]
        [StringLength(50, MinimumLength = 1)]
        public string name { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Created")]
        public DateTime created_at { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Last Updated")]
        public DateTime updated_at { get; set; }

        [Display(Name = "Created By")]
        public int? created_by { get; set; }

        [Display(Name = "Updated By")]
        public int? updated_by { get; set; }

        [ScaffoldColumn(false)]
        public int lock_version { get; set; }

        [Display(Name = "Program Number")]
        [StringLength(50, MinimumLength = 1)]
        public string program_number { get; set; }

    }
}