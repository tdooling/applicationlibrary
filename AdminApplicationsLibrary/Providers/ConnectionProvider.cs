﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminApplicationsLibrary.Providers
{
    public abstract class ConnectionProvider
    {

        internal static string dbContextString = "DbContext";

        internal static IDbConnection OpenConnection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings[dbContextString].ConnectionString;
            SqlConnection dbConnection = new SqlConnection(connectionString);
            return dbConnection;
        }

        public static void SetContextString(string context)
        {
            dbContextString = context;
        }
    
    }
}
