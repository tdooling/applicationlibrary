﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace AdminApplicationsLibrary.Providers
{
    class CCCMembershipProvider:MembershipProvider
    {

        //  CREATE TABLE [dbo].[users](
        //      [id] [int] IDENTITY(1,1) NOT NULL,
        //      [user_name] [varchar](50) NOT NULL,
        //      [password] [varchar](128) NOT NULL CONSTRAINT [DF_users_password]  DEFAULT ('aed32e7c6e59fcd5273421187338882fe5eb6eb1'),
        //      [first_name] [varchar](50) NOT NULL,
        //      [last_name] [varchar](50) NOT NULL,
        //      [email] [varchar](100) NOT NULL,
        //      [is_active] [bit] NOT NULL CONSTRAINT [DF__users__is_active__73852659]  DEFAULT ((1)),
        //      [last_login_date] [datetime] NULL CONSTRAINT [DF__users__last_logi__74794A92]  DEFAULT (NULL),
        //      [created_at] [datetime] NOT NULL CONSTRAINT [DF_users_created_at]  DEFAULT (getdate()),
        //      [updated_at] [datetime] NOT NULL CONSTRAINT [DF_users_updated_at]  DEFAULT (getdate()),
        //      [created_by] [int] NULL,
        //      [updated_by] [int] NULL,
        //      [lock_version] [int] NOT NULL CONSTRAINT [DF__users__lock_vers__7755B73D]  DEFAULT ((0)),
        //      [middle_initial] [varchar](50) NULL,
        //  CONSTRAINT [PK__users__72910220] PRIMARY KEY CLUSTERED 
        //    (
        //        [id] ASC
        //    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        //    ) ON [PRIMARY]

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            throw new NotImplementedException();
        }
    }
}
