﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdminApplicationsLibrary;
using AdminApplicationsLibrary.DAL;

namespace AdminApplicationsLibrary.Logging
{
    public static class Logger
    {
        public static void LogError(Exception ex)
        {
            var repository = new UCCCCExceptionRepository();


            if (repository != null)
            {
                var exception = new UCCCCException(ex);
                repository.Create(exception);
                repository = null;
            }
        }
    }
}
