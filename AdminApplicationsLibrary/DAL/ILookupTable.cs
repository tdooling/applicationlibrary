﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminApplicationsLibrary.DAL
{

    interface ILookupTable<T> where T : class
    {

        IEnumerable<T> Fill();

        T Find(int? id);

        bool Update(int? id, T obj);

        int Create(T obj);

        bool Delete(int? id, T obj);

    }

}
