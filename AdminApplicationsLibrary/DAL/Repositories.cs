﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminApplicationsLibrary.DAL
{

    public abstract class aCTRCMemberRepository : MemberInfoRepository<CTRC_Member>
    { }

    public abstract class aContactRepository : MemberInfoRepository<Contact>
    { }

    public abstract class aUniversityAppointmentRepository : MemberInfoRepository<University_Appointment>
    { }

    public abstract class aMemberListViewModelRepository : MemberInfoRepository<MemberListViewModel>
    {

        public virtual IEnumerable<MemberListViewModel> GetAllLeadership()
        {
            IEnumerable<MemberListViewModel> result = null;
            var members = base.Fill();
            var leaders = from data in members.ToList()
                          where data.is_program_leader || data.is_facility_director || data.is_cac_member
                          select data;
            result = leaders.ToList();
            return result;
        }

        public virtual IEnumerable<MemberListViewModel> GetAllResearch()
        {
            IEnumerable<MemberListViewModel> result = null;
            var members = base.Fill();
            var leaders = from data in members.ToList()
                          where data.is_clinical
                          select data;
            result = leaders.ToList();
            return result;
        }

    }

    public abstract class aMemberRepository : MemberInfoRepository<Member>
    { }

    public abstract class aMembershipRepository : MemberInfoRepository<Membership>
    { }

    public abstract class aCEC_MembershipRepository : MemberInfoRepository<CEC_Membership>
    { }

    public abstract class aCRAC_MembershipRepository : MemberInfoRepository<CRAC_Membership>
    { }

    public abstract class aCTRC_MemberRepository : MemberInfoRepository<CTRC_Member>
    { }

    public abstract class aSam_MemberRepository : MemberInfoRepository<Sam_Member>
    { }

    public abstract class aMembers_PublicationRepository : MemberInfoRepository<Members_Publication>
    { }

    public abstract class aMember_CommunicationRepository : MemberInfoRepository<Member_Communication>
    { }

    public abstract class aContacts_MemberRepository : MemberInfoRepository<Contacts_Member>
    { }

    public abstract class aRoles_UserRepository : MemberInfoRepository<Roles_User>
    { }

    public abstract class aCTRC_MembershipRepository : Repository<CTRC_Membership>
    { }

    public abstract class aSam_MembershipRepository : Repository<Sam_Membership>
    { }

    public abstract class aPublicationRepository : MemberInfoRepository<Publication>
    { }

    public abstract class aCTRC_Membership_TypeRepository : LookupTable<CTRC_Membership_Type>
    { }

    public abstract class aMember_Promotion_MediumRepository : LookupTable<Member_Promotion_Medium>
    { }

    public abstract class aMembership_StatusRepository : LookupTable<Membership_Status>
    { }

    public abstract class aDepartmentRepository : LookupTable<Department>
    { }

    public abstract class aRankRepository : LookupTable<Rank>
    { }

    public abstract class aProgramRepository : LookupTable<Program>
    { }

    public abstract class aSectionRepository : LookupTable<Section>
    { }

    public abstract class aRoleRepository : LookupTable<Role>
    { }

    public abstract class aResourceRepository : LookupTable<Resource>
    { }

    public abstract class aUserRepository : LookupTable<User>
    { }

    public class UCCCCExceptionRepository : LookupTable<UCCCCException>
    {
        public override int Create(UCCCCException obj)
        {
            return base.Create(obj);
        }
    }

}
