﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AdminApplicationsLibrary.DAL
{

    public abstract class WSConnection
    {
        static String _baseAddress = "";

        public string baseAddress
        {
            get
            {
                return _baseAddress;
            }
            set
            {
                _baseAddress = value;
            }
        }

        public WSConnection()
        {
            if (String.IsNullOrEmpty(baseAddress))
            {
                baseAddress = ConfigurationManager.AppSettings["PublicationsWS"];
            }
        }

        public WSConnection(String baseAddress)
        {
            this.baseAddress = baseAddress;
        }

        static public void SetBaseAddress(String baseAddress)
        {
            _baseAddress = baseAddress;
        }

        public virtual HttpClient CreateClient(string baseAddress, MediaTypeWithQualityHeaderValue header)
        {
            var client = new HttpClient();
            if (!string.IsNullOrEmpty(baseAddress))
                client.BaseAddress = new Uri(baseAddress);
            else
                client.BaseAddress = new Uri(this.baseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(header);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("scheme", "parameter");
            return client;
        }

        public virtual HttpClient CreateClient(string baseAddress, MediaTypeWithQualityHeaderValue header, AuthenticationHeaderValue authenticationHeader)
        {
            var client = new HttpClient();
            if (!string.IsNullOrEmpty(baseAddress))
                client.BaseAddress = new Uri(baseAddress);
            else
                client.BaseAddress = new Uri(this.baseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(header);
            client.DefaultRequestHeaders.Authorization = authenticationHeader;
            return client;
        }

    }

}
