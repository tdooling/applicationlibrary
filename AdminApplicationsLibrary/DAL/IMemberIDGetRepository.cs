﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdminApplicationsLibrary.DAL
{

    public interface IMemberIDGetRepository<T> where T : class
    {
        IEnumerable<T> FillByMemberId(int? memberId);
    }

}
